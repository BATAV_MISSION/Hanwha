﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Sales.Illustration.Web.Extensions;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.Services;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/

        public ActionResult Index()
        {
            return View();
        }

        [DeleteFile]
        public virtual FileResult Download()
        {
            var fullPath = Path.Combine(Server.MapPath("~/Report/temp"), _session.TransLogCode + ".pdf");
            var fi = new FileInfo(fullPath);

            return fi.Exists ? File(fullPath, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(fullPath)) : File(fullPath, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(RegenerateReport()));
        }

        [DeleteFile]
        public async Task<ActionResult> SendMail(SendMailViewModel model)
        {
            var message = new MailMessage();
            message.To.Add(new MailAddress(model.To));
            message.From = new MailAddress("sis@hanwha.co.id");
            message.Subject = model.Subject;
            message.Body = model.Body;
            message.IsBodyHtml = true;

            var fullPath = Path.Combine(Server.MapPath("~/Report/temp"), _session.TransLogCode + ".pdf");
            var fi = new FileInfo(fullPath);

            if (fi.Exists)
            {
                message.Attachments.Add(new Attachment(fullPath));
            }
            else
            {
                var path = RegenerateReport();
                message.Attachments.Add(new Attachment(path));
            }

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "dafied.dap@gmail.com",
                    Password = "ngawi24081993"
                };

                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
                message.Attachments.Dispose();

                if (model.Source.Equals("tra"))
                    return RedirectToAction("ResetAllData", "Traditional");
                else
                    return RedirectToAction("ResetAllData", "UnitLink");
            }
        }

        private string RegenerateReport()
        {
            SummaryViewModel dataInput = new SummaryViewModel();
            dataInput.Nasabah = _session.dataNasabah;
            dataInput.Premi = _session.premiInvestmentData;
            dataInput.Rider = _session.riderData;
            dataInput.Additional = _session.addInsData;
            dataInput.TopUp = _session.topUpWithData;
            dataInput.Insurance = _session.insCostRatioData;

            if (_session.addInsData != null)
                dataInput.Additional = _session.addInsData;
            else
                dataInput.Additional = new AdditionalInsuredViewModel();

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(dataInput.Insurance.Language);

            var result = MainServices.GenerateReport(dataInput, _session.AgentName, _session.AgentCode, _session.AppVersion);

            var rptH = new ReportDocument { FileName = Server.MapPath(string.Format("~/Report/{0}.rpt", result.ReportName)) };
            rptH.SetDataSource(result.IllustrationDataSet);

            var guid = _session.TransLogCode + ".pdf";
            var path = Path.Combine(Server.MapPath("~/Report/temp"), guid);
            rptH.ExportToDisk(ExportFormatType.PortableDocFormat, path);

            return path;
        }
    }
}
