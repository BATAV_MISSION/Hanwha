﻿using Sales.Illustration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Controllers
{
    [SessionExpire]
    public class ExitController : Controller
    {
        //
        // GET: /Exit/

        public ActionResult Index()
        {
            Session.Clear();

            return RedirectToAction("Index", "Home");
        }

    }
}
