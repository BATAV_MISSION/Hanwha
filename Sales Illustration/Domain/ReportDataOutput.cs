﻿using System;
using System.Data;

namespace Sales.Illustration.Web.Domain
{
    public class ReportDataOutput
    {
        public string ReportName { get; set; }
        public string[] ErrorMsg { get; set; }
        
        public DataSet IllustrationDataSet { get; set; }
    }
}
