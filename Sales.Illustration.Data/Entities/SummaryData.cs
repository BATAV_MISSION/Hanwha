﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class SummaryData
    {
        [Key]
        public long SummaryDataId { get; set; }
        public string TransCode { get; set; }
        public Nullable<decimal> TotalCOI { get; set; }
        public Nullable<double> COIRatio { get; set; }
        public string CultureId { get; set; }

        public virtual TransLog TransLog { get; set; }
    }
}
