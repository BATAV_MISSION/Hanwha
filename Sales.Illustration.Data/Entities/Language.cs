﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class Language
    {
        [Key]
        public string CultureId { get; set; }
        public string Name { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
