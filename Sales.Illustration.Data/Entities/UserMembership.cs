﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sales.Illustration.Data.Entities
{
    public class UserMembership
    {
        [Key]
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public Nullable<System.DateTime> JoinDate { get; set; }
        public string AAJILicense { get; set; }
        public string EncPass { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
