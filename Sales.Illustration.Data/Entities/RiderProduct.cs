﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class RiderProduct
    {
        [Key]
        public int RiderProductId { get; set; }
        public Nullable<int> RiderId { get; set; }
        public string ProductCode { get; set; }

        public virtual Product Product { get; set; }
        public virtual Rider Rider { get; set; }
    }
}
